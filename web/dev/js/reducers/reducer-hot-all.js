export const BlogsHotAllReducer = (state = [], action) => {
    switch (action.type) {
      case 'FETCH_HOTALL_SUCCESS':
            return action.hotall;
      default:
            return state;
    }
};