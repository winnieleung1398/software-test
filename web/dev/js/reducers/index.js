import {createStore, applyMiddleware} from 'redux';
import {combineReducers} from 'redux';
import {BlogsReducer} from './reducer-blog';
import MenuReducer from './reducer-menu';

import {BlogsHotNewsReducer} from './reducer-hot-news';
import {BlogsHotBlogsReducer} from './reducer-hot-blogs';
import {BlogsHotAllReducer} from './reducer-hot-all';
//import thunk from 'redux-thunk';

/*
 * We combine all reducers into a single object before updated data is dispatched (sent) to store
 * Your entire applications state (store) is just whatever gets returned from all your reducers
 * */

const middleware = applyMiddleware();


const allReducers = combineReducers({
    menu: MenuReducer,
    blogs: BlogsReducer,
    hotnews: BlogsHotNewsReducer,
    hotblogs: BlogsHotBlogsReducer,
    hotall: BlogsHotAllReducer
    
});

export default allReducers
