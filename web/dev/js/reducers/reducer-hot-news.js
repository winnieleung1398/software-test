export const BlogsHotNewsReducer = (state = [], action) => {
    switch (action.type) {
      case 'FETCH_HOTNEWS_SUCCESS':
            return action.hotnews;
      default:
            return state;
    }
};