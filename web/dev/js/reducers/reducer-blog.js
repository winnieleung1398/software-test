
export const BlogsReducer = (state = {blogsReducerBlogs : [] , blogsReducerLoadingMore : false}, action) => {
      switch (action.type) {
        case 'FETCH_BLOGS_SUCCESS':
        console.log(action);
              return {
                    blogsReducerBlogs :  action.curpage == 0 ? action.blogs : state.blogsReducerBlogs.concat(action.blogs),
                    blogsReducerLoadingMore : false
              }
        default:
              return state;
      }
  };