export const BlogsHotBlogsReducer = (state = [], action) => {
    switch (action.type) {
      case 'FETCH_HOTBLOGS_SUCCESS':
            return action.hotblogs;
      default:
            return state;
    }
};