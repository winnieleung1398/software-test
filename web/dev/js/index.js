import 'babel-polyfill';
import React from 'react';
import ReactDOM from "react-dom";
import {Provider} from 'react-redux';
//import {createStore} from 'redux';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
// import promise from 'redux-promise';
// import createLogger from 'redux-logger';
import allReducers from './reducers';

import * as blogActions from './actions/index';
import App from './components/App';

// const logger = createLogger();
const store = createStore(
    allReducers,
    applyMiddleware(thunk)
);

//const store = createStore(allReducers);

store.dispatch(blogActions.fetchBlogs());
store.dispatch(blogActions.fetchHotNews());
store.dispatch(blogActions.fetchHotBlogs());
store.dispatch(blogActions.fetchHotAll());


ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
