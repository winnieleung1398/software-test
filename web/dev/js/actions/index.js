import * as actionTypes from './actionTypes';
import Axios from 'axios';

let nodejsApi = "http://winnie007expressjs.kirinapp.com/"
let basehost = "http://winnie007api.kirinapp.com/";
let apiBaseUrl = basehost + "?action=";

// Sync Action
export const fetchBlogsSuccess = (blogs , curpage) => {
    return {
      type: 'FETCH_BLOGS_SUCCESS',
      blogs,
      curpage
    }
};

export const fetchHotNewsSuccess = (hotnews) => {
    return {
      type: 'FETCH_HOTNEWS_SUCCESS',
      hotnews
    }
};

export const fetchHotBlogsSuccess = (hotblogs) => {
    return {
      type: 'FETCH_HOTBLOGS_SUCCESS',
      hotblogs
    }
};
export const fetchHotAllSuccess = (hotall) => {
    return {
      type: 'FETCH_HOTALL_SUCCESS',
      hotall
    }
};

export const fetchBlogs = (page = 0, dropdownValue = -1) => {
    return (dispatch) => {
        return Axios.get(apiBaseUrl + "NEWS_FEED&page=" + page + "&size=10&tag=" + dropdownValue)
        .then(response => {
            setTimeout(() => {
                dispatch(fetchBlogsSuccess(response.data.data , page))
              }, 1000)
        })
        .catch(error => {
            throw(error);
        });
    };
};

export const fetchHotNews = () => {
    return (dispatch) => {
        return Axios.get(nodejsApi + 'listNews')
        .then(response => {
            dispatch(fetchHotNewsSuccess(response.data))
        })
        .catch(error => {
            throw(error);
        });
    };
};

export const fetchHotBlogs = () => {
    return (dispatch) => {
        return Axios.get(apiBaseUrl + 'HOT_BLOG&page=0&size=10')
        .then(response => {
            dispatch(fetchHotBlogsSuccess(response.data.data))
        })
        .catch(error => {
            throw(error);
        });
    };
};

export const fetchHotAll = () => {
    return (dispatch) => {
        return Axios.get(apiBaseUrl + 'HOT_ALL&page=0&size=10')
        .then(response => {
            dispatch(fetchHotAllSuccess(response.data.data))
        })
        .catch(error => {
            throw(error);
        });
    };
};