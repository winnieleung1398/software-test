import React, {Component} from 'react';

class LogoBar extends Component {
    render() {
        return (
            <div className="menu__mid">
                <div className="wrapper">
                    <a href="https://www.hk01.com/">
                        <div id="logo" className="float_left">
                            <img src="./images/logo_CNY.png" alt={"香港01"} />
                        </div>
                    </a>
                    <div className="menu__mid--current">
                        <a href="https://www.hk01.com/channel/01%E8%A7%80%E9%BB%9E">01觀點</a>
                    </div>
                    <div className="ads float_right">
                        <div classID="div-gpt-top-header-1"></div>
                    </div>
                    
                </div>
            </div>
        ); 
    }
}

export default LogoBar;