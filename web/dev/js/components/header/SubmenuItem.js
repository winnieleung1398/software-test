import React, {Component} from 'react';

class SubmenuItem extends Component {
    render() {
        return (
            <li key={this.props.navItem.id}><a href={this.props.navItem.url}>{this.props.navItem.cat}</a></li>
        ); 
    }
}

export default SubmenuItem;