import React, {Component} from 'react';
import TopBar from './TopBar';
import LogoBar from './LogoBar';
import NavBar from './NavBar';
import Submenu from './Submenu';

class Header extends Component {
    constructor(){
        super();
        this.state = {
            active: false
        }
        this.toggleClass = this.toggleClass.bind(this);
    }
    toggleClass(e){
        e.preventDefault()
        
        this.setState({ active: !this.state.active });
    }
    
    render() {
        return (
            <header className="nocontent">
                <TopBar />
                <LogoBar />
                <NavBar handler={this.toggleClass} active={this.state.active}/>
                <Submenu active={this.state.active}/>
            </header>
        ); 
    }
}

export default Header;