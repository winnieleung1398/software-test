import React, {Component} from 'react';
import axios from 'axios';


class Signup extends Component {
    constructor(props){
        super(props);
        this.state = {
            email:'',
            password:'',
            isInvalidValidPassword: false,
            isInvalidValidEmail: false
        }
        this.handlePwdChange = this.handlePwdChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }
    
    handleClick(event){
        var pass_regex = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+){6,12}$/g;
        var isValid = !(pass_regex.test(this.state.password));
        if(isValid){
            return this.setState({isInvalidValidPassword:true});
        }

        var email_regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var isValid = !(email_regex.test(this.state.email));
        if(isValid){
            return this.setState({isInvalidValidEmail:true});
        }

        let basehost = "http://winnie007api.kirinapp.com/";
        let apiMemberBaseUrl = basehost + "member.php?action=";
        var self = this;
        var headers = { 
            'Content-Type' : 'application/x-www-form-urlencoded',
        }
        var params = new URLSearchParams();
        params.append('username', this.state.email);
        params.append('password', this.state.password);
        if(!params){
            return;
        }
        console.log(params);
        axios.post(
            apiMemberBaseUrl + 'signup', 
            params
        ).then(function (response) {
         console.log(response);
         if(response.data.data == "USER_ADDED"){
            self.props.context.setState({active:false,isLogin:true,userinfo:{username:self.state.email}});
            console.log("USER_ADDED successfull");
         }else{
            self.props.context.setState({active:false,isLogin:false});
            console.log(response.data.data);
         }
       })
       .catch(function (error) {
         console.log(error);
       });
      }
    handleEmailChange(event) {
        this.setState({email: event.target.value});
    }
    handlePwdChange(event){
        this.setState({password: event.target.value , isInvalidValidPassword: false});
    }
    render() {
        return (
            <div>
                <div id="signup">
                    <h5 style={this.state.isInvalidValidPassword ? {'display':'block'}:{'display':'none'}}>Password (6-12 alpha and numberic)</h5>
                    <h5 style={this.state.isInvalidValidEmail ? {'display':'block'}:{'display':'none'}}>Email invalid</h5>
                    <input type="email" id="email" placeholder="Email" 
                    onChange={this.handleEmailChange}/>
                    <input type="password" id="password" placeholder="Password" 
                    onChange={this.handlePwdChange}/>
                    <button id="send" onClick={this.handleClick}>Send</button>
                </div>
            </div>
        ); 
    }
}

export default Signup;