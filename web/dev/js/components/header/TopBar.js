import React, {Component} from 'react';
import Login from './Login';
import Signup from './SignUp';

class TopBar extends Component {
    constructor(props){
        super(props);
        this.state = {
            active: false,
            signup:false,
            login:true,
            isLogin:false,
            userinfo:{}
        }
        this.membershipToggle = this.membershipToggle.bind(this);
        this.switchSignUp = this.switchSignUp.bind(this);
        this.memberLogout = this.memberLogout.bind(this);
    }
    membershipToggle(){
        this.setState({ active: !this.state.active });
    }
    switchSignUp(word){
        var signup,login;
        if(word == "signup"){signup = true;login = false;}
        else{login = true; signup = false;}
        return this.setState({login:login,signup:signup})
    }
    memberLogout(){
        this.setState({ active: false,
            signup:false,
            login:true,
            isLogin:false,
            userinfo:{} });
    }
    render() {
        var showStyle = (this.state.active)?{display: 'block'}:{};
        var showUserLogin = (this.state.isLogin)?{display: 'none'}:{};
        var showUserInfo = (!this.state.isLogin)?{display: 'none'}:{};
        var self = this;
        return (

                <div className="menu__top">
                    <div className="wrapper">
                        <div className="container">
                            <a id="popup_datetime">
                                <div className="weather">
                                    <span className="date">2017.1.29&nbsp;&nbsp;10:00</span>
                                    <span className="week">&nbsp;星期日&nbsp;&nbsp;</span>
                                    <span className="temp">&nbsp;19°C</span>
                                </div>
                            </a>
                            <div className="container float_right">
                                <a id="popup_search"><div className="search"><span></span></div></a>
                                <a id="popup_member" style={showUserLogin}>
                                    <div className="member_btn" onClick={this.membershipToggle}>
                                        <span></span>會員登入
                                    </div>
                                </a>
                                <a id="popup_member" style={showUserInfo}>
                                    <div className="member_btn" onClick={this.memberLogout}>
                                        <span></span>{this.state.userinfo.username} | 登出
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div className="menu__top__member menu__top_popup" style={showStyle}>
                            <div className="menu__top__member_beforelogin">
                                <ul>
                                    <li>
                                        <a className="btn alreadymember" onClick={self.switchSignUp.bind(null,"login")}>登入</a>
                                        {self.state.login? <Login context={self}/> : null}
                                    </li>
                                    <li>
                                        <a className="btn notyetmember" onClick={self.switchSignUp.bind(null,"signup")}>註冊成為會員</a>
                                        { self.state.signup? <Signup context={self}/> : null}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
        ); 
    }
}

export default TopBar;