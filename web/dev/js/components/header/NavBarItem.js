import React, {Component} from 'react';

class NavBarItem extends Component {
    render() {
        return (
            <li className="menu__main--tit"><a href="{this.props.navItem.url}">{this.props.navItem.name}</a></li>
        ); 
    }
}

export default NavBarItem;