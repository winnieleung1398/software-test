import React, {Component} from 'react';
import SubmenuItem from './SubmenuItem';

class Submenu extends Component {
    constructor(){
        super();
        this.state = {
            navItems: [{"id":0,"cat":"主頁","url":"http://www.hk01.com/"},{"id":1,"cat":"熱門文章","url":"http://www.hk01.com/module/hotnews"},{"id":2,"cat":"最新文章","url":"http://www.hk01.com/module/latestnews"},{"id":3,"cat":"新聞","url":"https://www.hk01.com/channel/%E6%96%B0%E8%81%9E"},{"id":4,"cat":"特首選舉","url":"http://2017hkceelection.hk01.com/"},{"id":5,"cat":"港聞","url":"https://www.hk01.com/section/%E6%B8%AF%E8%81%9E"},{"id":6,"cat":"兩岸","url":"https://www.hk01.com/section/%E5%85%A9%E5%B2%B8"},{"id":7,"cat":"經濟","url":"https://www.hk01.com/section/%E7%B6%93%E6%BF%9F"},{"id":8,"cat":"環保","url":"https://www.hk01.com/section/%E7%92%B0%E4%BF%9D"},{"id":9,"cat":"罪案","url":"https://www.hk01.com/section/%E7%BD%AA%E6%A1%88"},{"id":10,"cat":"社區","url":"https://www.hk01.com/channel/%E7%A4%BE%E5%8D%80"},{"id":11,"cat":"娛樂","url":"https://www.hk01.com/channel/%E5%A8%9B%E6%A8%82"},{"id":12,"cat":"電影","url":"https://www.hk01.com/channel/%E9%9B%BB%E5%BD%B1"},{"id":13,"cat":"音樂","url":"https://www.hk01.com/channel/%E9%9F%B3%E6%A8%82"},{"id":14,"cat":"韓迷","url":"https://www.hk01.com/channel/%E9%9F%93%E8%BF%B7"},{"id":15,"cat":"國際","url":"https://www.hk01.com/section/%E5%9C%8B%E9%9A%9B"},{"id":16,"cat":"女生","url":"https://www.hk01.com/channel/%E5%A5%B3%E7%94%9F"},{"id":17,"cat":"好生活","url":"https://www.hk01.com/section/%E5%A5%BD%E7%94%9F%E6%B4%BB"},{"id":18,"cat":"親子","url":"https://www.hk01.com/channel/%E8%A6%AA%E5%AD%90"},{"id":19,"cat":"寵物","url":"https://www.hk01.com/channel/%E5%AF%B5%E7%89%A9"},{"id":20,"cat":"01教煮","url":"https://www.hk01.com/channel/01%E6%95%99%E7%85%AE"},{"id":21,"cat":"體育","url":"https://www.hk01.com/section/%E9%AB%94%E8%82%B2"},{"id":22,"cat":"跑步","url":"https://www.hk01.com/section/%E8%B7%91%E6%AD%A5"},{"id":23,"cat":"熱話","url":"https://www.hk01.com/channel/%E7%86%B1%E8%A9%B1"},{"id":24,"cat":"科技玩物","url":"https://www.hk01.com/channel/%E7%A7%91%E6%8A%80%E7%8E%A9%E7%89%A9"},{"id":25,"cat":"01哲學","url":"http://philosophy.hk01.com/"},{"id":26,"cat":"01觀點","url":"https://www.hk01.com/section/01%E8%A7%80%E9%BB%9E"},{"id":27,"cat":"01博評","url":"https://www.hk01.com/blogger/01%E5%8D%9A%E8%A9%95"},{"id":28,"cat":"博評作者","url":"http://www.hk01.com/bloggers"},{"id":29,"cat":"01偵查","url":"https://www.hk01.com/channel/01%E5%81%B5%E6%9F%A5"},{"id":30,"cat":"01影像","url":"https://www.hk01.com/photostory"},{"id":31,"cat":"01 Video","url":"http://www.hk01.com/tag/9972"},{"id":32,"cat":"01活動","url":"https://www.hk01.com/section/01%E6%B4%BB%E5%8B%95"},{"id":33,"cat":"01空間","url":"http://space.hk01.com/"},{"id":34,"cat":"會員專區","url":"https://www.hk01.com/channel/%E6%9C%83%E5%93%A1%E5%B0%88%E5%8D%80"},{"id":35,"cat":"01議題","url":"https://www.hk01.com/issue"}]
        }
    }
    render() {
        // const divStyle = {
        //     top:0,
        //     left:0,
        //     z-index:999
        // };
        const displayNoneStyle = {
            display: 'none'
        }
        const displayBlockStyle = {
            display: 'block'
        }

        let submenuItems = this.state.navItems.map(navItem => {
            return (
                <SubmenuItem key={navItem.id} navItem={navItem}/>
            )
        });
        return (
            <nav id="touch_submenu" className="menu__submenu" style={(this.props.active ? displayBlockStyle: displayNoneStyle)}>
                <div id="touch_submenu_wrapper_container" className="wrapper">
                    <div className="touch_submenu touch_submenu_wrapper">
                        <div className="menu__submenu__search" data-search-form="">
                            <input type="search" name="keyword" placeholder="搜尋"/>
                            <div className="btn">搜尋</div>
                        </div>
                    </div> 
                    <ul className="menu__submenu--list">
                        {submenuItems}
                    </ul>
                    <div className="static_links">
                        <ul>
                            <li><a href="https://www.hk01.com/Pages/Hotline">01線報</a></li>
                            <li><a href="https://www.hk01.com/Pages/PrivacyTerms">私隱條例</a></li>
                            <li><a href="https://www.hk01.com/mediakit/index.html">廣告查詢</a></li>
                            <li><a href="https://www.hk01.com/Pages/ContactUs">聯絡我們</a></li>
							<li><a href="https://www.hk01.com/Pages/Recruitment">加入我們</a></li>
                        </ul>
                        <div className="socialmedia_channel">
                            <a href="https://www.facebook.com/hk01wemedia" target="_blank"><img src="./images/facebook_channel.png" width="50"/></a>
                            <a href="http://www.youtube.com/c/%E9%A6%99%E6%B8%AF01WeMedia" target="_blank"><img src="./images/youtube_channel.png" width="50"/></a>
                            <a href="https://www.instagram.com/hk01wemedia" target="_blank"><img src="./images/instagram_channel.png" width="50"/></a>
                        </div>
                    </div>
                </div>
            </nav>
        ); 
    }
}

export default Submenu;