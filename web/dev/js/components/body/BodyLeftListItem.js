import React, {Component} from 'react';

class BodyLeftListItem extends Component {
    render() {
        return (
            <div className="infinite-list-item blog_listing__item">
                <a href={this.props.blogItem.url}>
                    <div className="blog_listing__item__img ">
                        <img src={this.props.blogItem.img}/>
                        <div className="editor_pick">精選</div>
                    </div>
                    <div className="blog_listing__item__content">
                        <div className="blog_listing__item__content__tag">
                            <tag>{this.props.blogItem.tag}</tag>
                        </div>
                        <div className="blog_listing__item__content__tit">
                            <h3>{this.props.blogItem.title}</h3>
                        </div>
                        <div className="blog_listing__item__content__caption">{this.props.blogItem.caption}</div>
                        <div className="blog_listing__item__content__time"><span className="clock">{this.props.blogItem.date}</span></div>
                    </div>
                </a>
            </div>
        ); 
    }
}

export default BodyLeftListItem;