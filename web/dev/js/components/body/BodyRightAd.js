import React, {Component} from 'react';

class BodyRightAd extends Component {
    render() {
        const adStyle = {
            border: '0pt none'
        };
        const adIframeStyle = {
            border: "0px",
            verticalAlign: "bottom"
        };
        return (
            <div className="ads ads_300x250">
                <div id="div-gpt-left-above-1" data-google-query-id="CNe5l_qg5tECFYYvlgod8ocIjg">
                    <div id="google_ads_iframe_/108346865/01Views_MREC_1_0__container__" style={adStyle}>
                        <img src="./images/testImg.png" alt={"香港01"} />
                    </div>
                </div>
            </div>
        ); 
    }
}

export default BodyRightAd;