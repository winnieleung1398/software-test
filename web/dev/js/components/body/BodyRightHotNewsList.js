import React, {Component} from 'react';
import BodyRightHotNewsListItem from './BodyRightHotNewsListItem';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import {fetchHotNews, fetchHotBlogs, fetchHotAll} from '../../actions/index'

class BodyRightHotNewsList extends Component {
    constructor(props){
        super(props);
        this.state = {
            currentTab: 1
        }
    }
    hotMenuToggle(index, event){
        this.setState({ currentTab: index });
    }
    render() {
        let hotnewsItems;
        let hotblogItems;
        let hotallItems;

        const displayNoneStyle = {
            display: 'none'
        }
        const displayBlockStyle = {
            display: 'block'
        }

        if (this.props.hotnews){
            hotnewsItems = this.props.hotnews.map(hotnewsItem => {
                return (
                    <BodyRightHotNewsListItem key={hotnewsItem.id} hotItem={hotnewsItem} />
                )
            })
        }

        if (this.props.hotblogs){
            hotblogItems = this.props.hotblogs.map(hotblogItem => {
                return (
                    <BodyRightHotNewsListItem key={hotblogItem.id} hotItem={hotblogItem} />
                )
            })
        }

        if (this.props.hotall){
            hotallItems = this.props.hotall.map(hotallItem => {
                return (
                    <BodyRightHotNewsListItem key={hotallItem.id} hotItem={hotallItem} />
                )
            })
        }

        return (
            <div className="sidebar__hotnews">
                <a href="https://www.hk01.com/module/hotnews">
                    <div className="border_tit sidebar__hotnews__tit">熱門新聞排行榜</div>
                </a>
                <div className="tab_hotnews">
                    <div className={'tab_hotnews__item'+(this.state.currentTab == 1?' current':'')} id="news" onClick={this.hotMenuToggle.bind(this, 1)}>新聞</div>
                    <div className={'tab_hotnews__item'+(this.state.currentTab == 2?' current':'')} id="blog" onClick={this.hotMenuToggle.bind(this, 2)}>博評</div>
                    <div className={'tab_hotnews__item'+(this.state.currentTab == 3?' current':'')} id="all" onClick={this.hotMenuToggle.bind(this, 3)}>01</div>
                </div>
                <ul className="hotnews__container hotnews--news" style={(this.state.currentTab == 1 ? displayBlockStyle: displayNoneStyle)}>
                    {hotnewsItems}
                </ul>
                <ul className="hotnews__container hotnews--blog" style={(this.state.currentTab == 2 ? displayBlockStyle: displayNoneStyle)}>
                    {hotblogItems}
                </ul>
                <ul className="hotnews__container hotnews--all" style={(this.state.currentTab == 3 ? displayBlockStyle: displayNoneStyle)}>
                    {hotallItems}
                </ul>
            </div>
        ); 
    }
}

function mapStateToProps(state) {
    return {
        hotnews: state.hotnews,
        hotblogs: state.hotblogs,
        hotall: state.hotall

    };
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(BodyRightHotNewsList);