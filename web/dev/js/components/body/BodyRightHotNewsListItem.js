import React, {Component} from 'react';

class BodyRightHotNewsListItem extends Component {
    render() {
        return (
            <li className="">
                <b>{this.props.hotItem.id+1}</b>
                <a href={this.props.hotItem.url}>
                    <span><h3>{this.props.hotItem.title}</h3></span>
                    <img src={this.props.hotItem.img}/>
                </a>
            </li>
        ); 
    }
}

export default BodyRightHotNewsListItem;