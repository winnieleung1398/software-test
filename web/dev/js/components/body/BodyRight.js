import React, {Component} from 'react';
import BodyRightAd from './BodyRightAd';
import BodyRightHotNewsList from './BodyRightHotNewsList';

class BodyRight extends Component {
    render() {
        return (
            <div className="sidebar">
                <BodyRightAd />
                <BodyRightAd />
                <BodyRightHotNewsList />
            </div>
        ); 
    }
}

export default BodyRight;