import React, {Component} from 'react';
import BodyLeftCover from './BodyLeftCover';
import BodyLeftList from './BodyLeftList';


class BodyLeft extends Component {
    constructor(){
        super();
    }
    render() {
        return (
            <div className="wrapper-col">
                <div className="views_tit" id="channel_title_sticky">
                    <h1><span>觀</span><span>點</span></h1>
                </div>
                <BodyLeftCover />
                <BodyLeftList />
            </div>
        ); 
    }
}

export default BodyLeft;