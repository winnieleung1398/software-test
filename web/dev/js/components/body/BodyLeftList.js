import React, {Component} from 'react';
import BodyLeftListItem from './BodyLeftListItem';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import {fetchBlogs} from '../../actions/index'

let createHandlers = function(dispatch) {
    let onLoad = function(page, dropdownValue ) {
      dispatch(fetchBlogs(page, dropdownValue))
    };
    return {
        onLoad,
    };
  }

class BodyLeftList extends Component {
    constructor(props){
        super(props);
        this.state = {
            endofscreen: false,
            loadingMore: false,
            page : 0,
            dropdownValue: -1
        }
        this.handleWindowScroll = this.handleWindowScroll.bind(this);
        this.handlers = createHandlers(this.props.dispatch);
        this.onTargetSelect = this.onTargetSelect.bind(this);
    }
    onTargetSelect(event){
        // console.log("Outputting from here: ", target);
        // document.getElementById("dropdown-title").textContent(target);
        // this.props.passTargetToParent(target);
        //alert(event.target.value);
        this.setState({dropdownValue: event.target.value , page : 0});
        var page = 0;
        var dropdownValue = event.target.value;
        this.handlers.onLoad(page, dropdownValue);
    }
    loadMore() {
        console.log('loading More');
        if(this.state.loadingMore){
            return;
        }
        this.setState({
            loadingMore: true, 
            page : this.state.page + 1}
        );
        var page = this.state.page;
        var dropdownValue = this.state.dropdownValue;

        this.handlers.onLoad(page, dropdownValue);
    }
    
    componentWillReceiveProps(nextProps) {
        this.setState({loadingMore: false});
    }

    componentDidMount() {
        window.addEventListener("scroll", this.handleWindowScroll);
    }
    
    componentWillUnmount() {
        window.removeEventListener("scroll", this.handleWindowScroll);
    }

    handleWindowScroll() {
        var self = this;
        const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
        const body = document.body;
        const html = document.documentElement;
        const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight,  html.scrollHeight, html.offsetHeight);
        const windowBottom = windowHeight + window.pageYOffset;
        if (windowBottom >= docHeight) {
            console.log("end of screen")
            self.loadMore();
        } 
    }

    _renderBlogs() {
        let blogItems;
        if(this.props.blogs){
            blogItems = this.props.blogs.map(blogItem => {
                return (
                    <BodyLeftListItem key={blogItem.id} blogItem={blogItem} />
                )
            })
        }
        return blogItems;
      }
    render() {
        return (
            <div className=" ">
                <div className="filter">
                    請選擇你感興趣的題目：
                    <select id="filter-dropdown" onChange={this.onTargetSelect} value={this.state.dropdownValue}>
                        <option value="-1">Select</option>
                        <option value="01觀點">01觀點</option>
                        <option value="01倡議">01倡議</option>
                        <option value="01札記">01札記</option>
                    </select>
                    {this.state.value}
                </div>
                <section className="blog_listing large_img" >
                {this._renderBlogs()}
                </section>
            </div>
        ); 
    }
}

function mapStateToProps(state) {
    return {
        blogs: state.blogs.blogsReducerBlogs,
        blogsReducerLoadingMore : state.blogs.blogsReducerLoadingMore
    };
}

export default connect(mapStateToProps)(BodyLeftList);