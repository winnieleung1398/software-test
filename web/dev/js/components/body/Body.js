import React, {Component} from 'react';
import BodyLeft from './BodyLeft';
import BodyRight from './BodyRight';

class Body extends Component {
    render() {
        return (
            <section className="channel channel-views">
                <div className="page_overlay_arrows"></div>
                <div className="wrapper">
                    <BodyLeft />
                    <BodyRight />
                </div>
            </section>
        ); 
    }
}

export default Body;