import React, {Component} from 'react';

class FooterItem extends Component {
    render() {
        return (
            <a href={this.props.footerItem.url}>{this.props.footerItem.name}</a>
        ); 
    }
}

export default FooterItem;