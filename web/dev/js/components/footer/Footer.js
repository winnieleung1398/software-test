import React, {Component} from 'react';
import FooterItem from './FooterItem';

class Footer extends Component {
    constructor(){
        super();
        this.state = {
            footerItems: [{"id":0,"name":"春節","url":"https://www.hk01.com/tag/6239"},{"id":1,"name":"年宵市場","url":"https://www.hk01.com/tag/5292"},{"id":2,"name":"甘比","url":"https://www.hk01.com/tag/9890"},{"id":3,"name":"Wonder Girls","url":"https://www.hk01.com/tag/1479"},{"id":4,"name":"TSA","url":"https://www.hk01.com/tag/220"},{"id":5,"name":"韋家雄","url":"https://www.hk01.com/tag/5944"},{"id":6,"name":"金高恩","url":"https://www.hk01.com/tag/8616"},{"id":7,"name":"森美","url":"https://www.hk01.com/tag/3708"},{"id":8,"name":"奧巴馬功過","url":"https://www.hk01.com/tag/13519"},{"id":9,"name":"特朗普","url":"https://www.hk01.com/tag/1270"},{"id":10,"name":"傅家俊","url":"https://www.hk01.com/tag/2480"},{"id":11,"name":"我是歌手","url":"https://www.hk01.com/tag/3357"},{"id":12,"name":"林憶蓮","url":"https://www.hk01.com/tag/2968"},{"id":13,"name":"BIGBANG","url":"https://www.hk01.com/tag/1444"},{"id":14,"name":"曹星如","url":"https://www.hk01.com/tag/2474"},{"id":15,"name":"曾俊華","url":"https://www.hk01.com/tag/30"},{"id":16,"name":"林鄭月娥","url":"https://www.hk01.com/tag/29"},{"id":17,"name":"劉德華墮馬","url":"https://www.hk01.com/tag/14825"},{"id":18,"name":"劉德華","url":"https://www.hk01.com/tag/1319"},{"id":19,"name":"2017施政報告","url":"https://www.hk01.com/tag/14460"},{"id":20,"name":"葉劉淑儀","url":"https://www.hk01.com/tag/92"},{"id":21,"name":"每逢大事講故事","url":"https://www.hk01.com/tag/13633"},{"id":22,"name":"西九故宮","url":"https://www.hk01.com/tag/14671"},{"id":23,"name":"曾蔭權涉貪案","url":"https://www.hk01.com/tag/14605"},{"id":24,"name":"Pokemon GO","url":"https://www.hk01.com/tag/11192"}]
            //footerItems: [{"name":"春節","url":"https://www.hk01.com/tag/6239"}]
        }
    }
    render() {
        const bn_adsStyle = {
            display: "none",
            opacity: 1
        };
        const footerLowerStyle = {
            height: "0.5px"
        };

        let footerItems = this.state.footerItems.map(footerItem => {
            return (
                <FooterItem key={footerItem.id} footerItem={footerItem}/>
            )
        });
        return (
            <footer className="nocontent">
                <div id="bn_ads" className="bn_ads sticky_0" style={bn_adsStyle} data-banner="https://www.hk01.com/assets/images/dfp_overlay_banner.jpg">
                    <div><div>X</div></div>
                </div>

    
                <div className="footer__upper">
                    <div className="footer__upper--header">
                        <div className="wrapper">
                                                            <div className="footer__upper__tag">
                                <span>熱門</span>
                                {footerItems}

                                                </div>
                                        </div>
                    </div>

                </div>
                <div className="footer__lower" style={footerLowerStyle}>
                    <div className="wrapper">
                        <div className="footer__lower__soical_icon float_left">
                            <a href="https://www.facebook.com/hk01wemedia" target="_blank"><div className="footer__lower__soical_icon__item icon_fb"></div></a>
                            <a href="https://www.instagram.com/hk01wemedia" target="_blank"><div className="footer__lower__soical_icon__item icon_instagram"></div></a>
                            <a href="http://www.youtube.com/c/%E9%A6%99%E6%B8%AF01WeMedia" target="_blank"><div className="footer__lower__soical_icon__item icon_youtube"></div></a>
                        </div>
                        <div className="footer__lower__link float_right">
                            <div className="footer__lower__link__container">
                                <span className="footer__lower__link-link"><a href="https://www.hk01.com/Pages/Hotline">01線報</a></span>
                                <span className="footer__lower__link-link"><a href="https://www.hk01.com/article/67022">重要聲明</a></span>
                                <span className="footer__lower__link-link"><a href="https://www.hk01.com/Pages/PrivacyTerms">私隱條例</a></span>
                                <span className="footer__lower__link-link"><a target="_blank" href="https://www.hk01.com/mediakit/index.html">廣告查詢</a></span>
                                <span className="footer__lower__link-link"><a href="https://www.hk01.com/Pages/ContactUs">聯絡我們</a></span>
                                <span className="footer__lower__link-link"><a href="https://www.hk01.com/Pages/Recruitment">加入我們</a></span>

                            </div>
                            <div className="footer__lower__link-txt">香港零一媒體有限公司版權所有 © 2017</div>
                        </div>
                    </div>
                </div>
            </footer>
        ); 
    }
}

export default Footer;