import React from 'react';
// import BlogList from '../containers/blog-list';

// import BlogDetail from '../containers/blog-detail';

// import TopBar from '../components/header/TopBar';
import Header from '../components/header/Header';
import Footer from '../components/footer/Footer';
import Body from '../components/body/Body';


// const App = () => (
//     <div>
//         <h2>User List</h2>
//         <UserList />
//         <hr />
//         <h2>User Details</h2>
//         <UserDetails />
//     </div>
// );

// const App = () => (
//     <div>
//         <BlogDetail />
//         <BlogList />

//     </div>
// )

class App extends React.Component {
    render(){
        return (
            <div>
                <Header/>
                <div className="wrapper">
                    <div className="ads ads_large_ads">
                        <div id="div-gpt-top-banner">
                            <div>
                                <img src="./images/largeAd.png" />
                            </div>
                        </div>
                    </div>
                </div>
                <Body/>
                <Footer/>
            </div>
        );
    }
}



export default App;
