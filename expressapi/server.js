var express = require('express');
var app = express();
var fs = require("fs");

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // solve CORS issue
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/listNews', function (req, res, next) { // get hot new in right side bar
   fs.readFile( __dirname + "/" + "news.json", 'utf8', function (err, data) {
       res.end( data );
   });
})

var server = app.listen(2000, function () {
  var host = server.address().address
  var port = server.address().port

  console.log("hot news API at http://%s:%s", host, port)

})