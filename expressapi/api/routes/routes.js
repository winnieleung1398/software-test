'use strict';

module.exports = function(app) {
  var getList = require('../controllers/controller');

  // api Routes
  app.route('/lists')
    .get(getList.list_all_news);


  app.route('/lists/:taskId')
    .get(getList.read_a_task);
};
