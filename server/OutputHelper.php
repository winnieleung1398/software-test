<?php 

class OutputHelper {
    
    private static $version = '1.0';
    
    public static function onSuccess($data, $reason = "") {
        $result = [];
        $result['result'] = true;
        $result['reason'] = $reason;
        $result["data"] = $data;
        $result['ts'] = time();
        self::output($result);
    }

    private static function output($output) {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Cache-Control: no-cache, must-revalidate');
        header("Content-Type: application/json", TRUE);
        echo json_encode(($output), JSON_HEX_APOS | JSON_HEX_QUOT);
        exit();
    }

}