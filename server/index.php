<?php

    include_once 'data.php';
    include_once 'DataHelper.php';
    include_once 'OutputHelper.php';

    $newsfeedsHelper = DataHelper::start($newsfeeds);
    $hotnewsNewsHelper = DataHelper::start($hotnews_news);
    $hotnewsBlogHelper = DataHelper::start($hotnews_blog);
    $hotnewsAllHelper = DataHelper::start($hotnews_all);

    $action = $_GET['action'] ?: 'NONE';
    $page = $_GET['page'] ?: 0;
    $size = $_GET['size'] ?: 2;
    $tag = $_GET['tag'] ?: -1;

    if($action === 'NEWS_FEED'){
        $data = $newsfeedsHelper->infinityFakePagination($page , $size , $tag);
    }else if($action === 'HOT_NEWS'){
        $data = $hotnewsNewsHelper->pagination($page, $size);
    }else if($action === 'HOT_BLOG'){
        $data = $hotnewsBlogHelper->pagination($page, $size);
    }else if($action === 'HOT_ALL'){
        $data = $hotnewsAllHelper->pagination($page, $size);
    }else {
        $data = [];
    }

    OutputHelper::onSuccess($data);