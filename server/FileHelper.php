<?php

class FileHelper {
  
    private $handle;
    private $file;

    public static function get($file){
        $f = new FileHelper;
        $f->load($file);
        return $f;
    }

    public function load($file_url)
    {
        $this->file = $file_url;
        if ($this->handle = fopen($file_url, 'a+')) {
            return $this;
        }
    }

    public function write($text)
    {
        if (fwrite($this->handle, $text . "\n" )) {
            fclose($this->handle);
            return true;
        } else {
            fclose($this->handle);
            return false;
        }
    }

    public function read()
    {
        if ($read = fread($this->handle, filesize($this->file))) {
            fclose($this->handle);
            return $read;
        } else {
            fclose($this->handle);
            return false;
        }
    }

    public function delete()
    {
        fclose($this->handle);
        if (file_exists($this->file)){
            if (unlink($this->file))
                return true;
            else
                return false;
        }
    }

}