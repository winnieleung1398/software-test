<?php

    include_once 'OutputHelper.php';
    include_once 'MemberHelper.php';

    $action = $_GET['action'] ?: 'NONE';

    if(!isset($_POST['username']) || !isset($_POST['password'])){
        OutputHelper::onSuccess('DATA_INVALID');
    }

    if($action === 'login'){
        $data = MemberHelper::login($_POST['username'], $_POST['password']);
    }else if($action === 'signup'){
        $data = MemberHelper::signup($_POST['username'], $_POST['password']);
    }else{
        $data = [];
    }

    OutputHelper::onSuccess($data);