<?php

class DataHelper {
    
    private $myData = [];

    public static function start($datas = []){
        $obj = new DataHelper;
        $obj->init($datas);
        return $obj;
    }

    public function init($datas = []){
        $this->myData = $datas;
    }
    public function all(){
        return $this->myData;
    }

    public function infinityFakePagination($page , $limit = 2, $tag = -1){
        $output = [];
        $start = ($page * $limit);
        $is_over_limit = $start > $this->count();
        $start = $is_over_limit ? $start % $this->count()  : $start;
        $end = ($start + $limit) > $this->count() ? $this->count() : ($start + $limit);
        for($i = $start  ; $i < $end; $i++){
            if($is_over_limit)  
                $this->myData[$i]['id'] = $i + ($page * $limit);
            if ($tag == -1 || $tag != -1 && $tag == $this->myData[$i]['tag'])
                $output[] = $this->myData[$i];
        }
        return $output;
    }

    public function pagination($page , $limit = 2){
        $output = [];
        $start = ($page * $limit);
        $end = ($start + $limit) > $this->count() ? $this->count() : ($start + $limit);
        for($i = $start  ; $i < $end; $i++){
            $output[] = $this->myData[$i];
        }
        return $output;
    }
    public function count(){
        return count($this->myData);
    }

}