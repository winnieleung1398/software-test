<?php

require_once 'FileHelper.php';

class MemberHelper {

    public static function checkMember($username , $password){
        $db = FileHelper::get('members.db');
        $datas = $db->read();
        $members = explode("\n",$datas);
        foreach($members as $member){
            list($_username, $_password) = explode(":",$member);
            if($_username === $username){
                if($_password === $password){
                    return 'SUCCESS';
                }else{
                    return 'PASSWORD_INVALID';
                }
            }
        }
        return 'NO_SUCH_USER';
    }
  
    public static function login($username , $password){
        $result = self::checkMember($username , $password);
        return $result;
    }

    public static function signup($username , $password){
        $result = self::checkMember($username , $password);
        if($result === 'NO_SUCH_USER'){
            $db = FileHelper::get('members.db');
            $db->write("$username:$password");
            return 'USER_ADDED';
        }
        return 'USER_EXIST';
    }

}